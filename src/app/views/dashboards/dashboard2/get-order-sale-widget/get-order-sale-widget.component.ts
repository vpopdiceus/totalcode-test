import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormControl, FormGroup} from '@angular/forms';
import { ApiServiceService } from '../../../../api-service.service';

@Component({
  selector: 'app-get-order-sale-widget',
  templateUrl: './get-order-sale-widget.component.html',
  styleUrls: ['./get-order-sale-widget.component.scss'],
})

export class GetOrderSaleWidgetComponent implements OnInit {
    items: Object[] = [{value: 'dt', text:'Today'},
        {value: 'dy', text:'Yesterday'},
        {value: 'dtw', text:'This Week'},
        {value: 'dlw', text:'Last Week'},
        {value: 'dtm', text:'This Month'},
        {value: 'dlm', text:'Last Month'},
        {value: 'all', text:'All'}];
    default: string = 'Today';
    period: string = 'dy';
    total: number = null;
    extra: number = null;
    constructor(private ApiService: ApiServiceService, private fb: FormBuilder) {

    }
    Form: FormGroup;

  ngOnInit() {
      this.Form = this.fb.group({
          salesWidget: ['dy'],
          salesValue: ['1'],
      });

  }
    onSubmit() {
        this.period = this.Form.value.salesWidget;
        this.ApiService.getSales(this.Form.value).subscribe((data: any) => {
            this.total = (data.data && data.data.coreData[0] && data.data.coreData[0].count[this.period]) ?
                data.data.coreData[0].count[this.period]: 0;
            this.extra = (data.data && data.data.extra[0] && data.data.extra[0].count[this.period]) ?
                data.data.extra[0].count[this.period]: 0;
        })
    }
}
