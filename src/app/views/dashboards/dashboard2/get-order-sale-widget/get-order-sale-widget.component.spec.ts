import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GetOrderSaleWidgetComponent } from './get-order-sale-widget.component';

describe('GetOrderSaleWidgetComponent', () => {
  let component: GetOrderSaleWidgetComponent;
  let fixture: ComponentFixture<GetOrderSaleWidgetComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GetOrderSaleWidgetComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GetOrderSaleWidgetComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
