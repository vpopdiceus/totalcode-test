import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class ApiServiceService {
    private baseUrl: string = 'https://uat-voucher-shop-service.totalcoding-test2.com/';
    private _salesURL: string = "api/dashboard/GetOrderSaleWidget";
    private _salesGraphURL: string = "api/dashboard/GetOrderSaleWidget";
    private _voucherSalesURL: string = "api/dashboard/GetOrderSaleWidget";
    private token: string = "Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1bmlxdWVfbmFtZSI6InByb3ZpZGVyX2FkbWluIiwibmFtZWlkIjoiYTUyZWU1YTYtMDE4OC00YTMyLTk4NmMtMjIxNjY3NjJhZDIyIiwiaHR0cDovL3NjaGVtYXMubWljcm9zb2Z0LmNvbS93cy8yMDA4LzA2L2lkZW50aXR5L2NsYWltcy91c2VyZGF0YSI6IntcIlRlbmFudElkXCI6MCxcIlRlbmFudEdVSURcIjpcIjAwMDAwMDAwLTAwMDAtMDAwMC0wMDAwLTAwMDAwMDAwMDAwMFwiLFwiQnJhbmROYW1lXCI6bnVsbCxcIkRlZmF1bHRMYW5ndWFnZUNvZGVcIjpudWxsLFwiRGVmYXVsdExhbmd1YWdlTmFtZVwiOm51bGwsXCJBY3RpdmVEb21haW5OYW1lXCI6bnVsbCxcIlVzZXJJZFwiOlwiYTUyZWU1YTYtMDE4OC00YTMyLTk4NmMtMjIxNjY3NjJhZDIyXCIsXCJVc2VyTmFtZVwiOlwicHJvdmlkZXJfYWRtaW5cIixcIlVzZXJEZXNjcmlwdGlvblwiOlwiUHJvdmlkZXIgQWRtaW5cIn0iLCJyb2xlIjpbIkNVU1QtQUNDRVNTIiwiQ1VTVC1SRUctRk9STSIsIkNVU1QtQ1JFQVRFLVRFU1QiLCJDVVNULVJFU0VULVBXRCIsIkNVU1QtRURJVCIsIkNVU1QtU1VTUEVORCIsIkNVU1QtQkxPQ0siLCJDVVNULU5PVEUiLCJDVVNULUFDVElWSVRZIiwiQ1VTVC1MSU5LRUQiLCJDVVNULUFHRS1SRVNUUklDVCIsIkNVU1QtS1lDIiwiVE5ULUFDQ0VTUyIsIlROVC1DUkVBVEUiLCJUTlQtRURJVCIsIlROVC1SRVBPUlQiLCJUTlQtRE9DVU1FTlQiLCJUTlQtUFJPRFVDVCIsIlROVC1JTlZPSUNFIiwiVE5ULVNFQVJDSCIsIkhMUC1BQ0NFU1MiLCJITFAtQ1JFQVRFLVRJQ0tFVCIsIkhMUC1BVFRBQ0gtRklMRSIsIkhMUC1ERUxFVEUtRklMRSIsIkhMUC1GT1JXQVJELVRJQ0tFVCIsIkhMUC1BREQtV0FUQ0hFUiIsIkhMUC1NRVJHRS1USUNLRVQiLCJITFAtRE9XTkxPQUQiLCJITFAtQUdFTlQtTUdSIiwiSExQLUFHRU5ULUdST1VQIiwiSExQLUFHRU5ULVJPTEUiLCJITFAtU0xBIiwiSExQLUNBTk5FRC1NR1IiLCJITFAtUlVMRS1NR1IiLCJITFAtQ1VTVC1TQVQtTUdSIiwiUFZSLUFDQ0VTUyIsIlBWUi1FRElUIiwiU1RBRkYtQ1JFQVRFIiwiU1RBRkYtRURJVCIsIlNUQUZGLVJFU0VULVBXRCIsIlNUQUZGLUJMT0NLIiwiU1RBRkYtQUNDRVNTIiwiUk9MRS1BQ0NFU1MiLCJST0xFLUVESVQtUFZSIiwiUk9MRS1FRElULVROVCIsIlJPTEUtRURJVC1GSVhFRCIsIlJPTEUtVVNFUiIsIlJPTEUtTElTVCIsIlJPTEUtQ0xPTkUiLCJDT05ULUFDQ0VTUyIsIlBSRC1BQ0NFU1MiLCJQUkQtREVMRVRFIiwiUFJELUNSRUFURS1VUERBVEUiLCJQUkRHUlAtQUNDRVNTIiwiUFJER1JQLURFTEVURSIsIlBSREdSUC1DUkVBVEUtVVBEQVRFIiwiT0RSLUFDQ0VTUyIsIkRBU0hCT0FSRC1TQUxFUyIsIlBST1ZJREVSIl0sIm5iZiI6MTUzNzI2MDQwNiwiZXhwIjoxNTM3MzQ2ODA2LCJpYXQiOjE1MzcyNjA0MDYsImlzcyI6IlRvdGFsQ29kaW5nIiwiYXVkIjoiaHR0cHM6Ly91YXQtdm91Y2hlci1zaG9wLXNlcnZpY2UudG90YWxjb2RpbmctdGVzdDIuY29tICJ9.73HF_gk0tEAXhdMXZJp-zuv1hQBUegCiEuE_ueZh7I0";
    private contenType : string = "application/json-patch+json";

    constructor(private http: HttpClient) {
    }

    getSales(values: any): Observable<Object> {
        const requestOptions = {
            headers: new HttpHeaders(
                {'Authorization': this.token,
                'Content-Type': this.contenType}),
        };
        return this.http.post(`${this.baseUrl}${this._salesURL}`,
            {tenantId: 1, dateType: values.salesWidget, argument: values.salesValue }
            , requestOptions);
    }

    getSalesGraph(value: string): Observable<Object> {
        const requestOptions = {
            headers: new HttpHeaders(
                {'Authorization': this.token,
                    'Content-Type': this.contenType}),
        };
        return this.http.post(`${this.baseUrl}${this._salesGraphURL}`,
            {tenantId: 1, dateType: value, argument: '' }
            , requestOptions);
    }

    getVoucherSalesGraph(value: string): Observable<Object> {
        const requestOptions = {
            headers: new HttpHeaders(
                {'Authorization': this.token,
                    'Content-Type': this.contenType}),
        };
        return this.http.post(`${this.baseUrl}${this._voucherSalesURL}`,
            {tenantId: 1, dateType: value, argument: '' }
            , requestOptions);
    }

}
