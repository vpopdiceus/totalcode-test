export interface salesTotals {
  salesId: number;
  id: number;
  currentRange: string;
  count: number;
}
